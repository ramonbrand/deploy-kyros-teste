# Desafio 1 - B
## Deploy no Google Cloud Plataform - Cloud Run
* Crie um novo projeto:
![](https://i.imgur.com/fwbeBMt.png)
![](https://i.imgur.com/43WZ6ch.png)

* Após criar o projeto crie um arquivo chamado de dockerfile na raiz do projeto de teste com o seguinte script:


```
FROM python:3.9-slim

ENV PYTHONUNBUFFERED True

ENV APP_HOME /app
WORKDIR $APP_HOME
COPY . ./

RUN pip install --no-cache-dir -r requirements.txt

CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 --timeout 0 app:app

```


OBS: A aplicação precisa estar na porta 8080 que é essa a porta que o google cloud usa por padrão, e qual iremos usar. 

* Para a próxima etapa é necessário criar o build da imagem docker do Dockerfile, no terminal execute o comando:



```
docker build -t kyros/flask:1.0
```

* Nessa etapa vamos trocar a tag para que possamos fazer o push do build para o google cloud. Para isso é necessário saber o id do projeto que foi criado no google cloud. o id fica indicado ao lado do nome do seu projeto, no mesmo botão que clicamos para criar ele:
![](https://i.imgur.com/RZsXQwb.png)

* para trocar a tag do build execute o comando abaixo:


```
$ docker tag teste/flask:1.0 gcr.io/kyros-ramon-teste/<nome do id aqui>
```


exemplo:


```
$ docker tag teste/flask:1.0 gcr.io/kyros-ramon-teste/image-flask-kyros

```


* Faça o push do build para o google cloud com o comando:


```
$ docker push gcr.io/kyros-ramon-teste/img-kyros 
```

* No container registry clique em imagens para visualizar o push da build
![](https://i.imgur.com/JhH2Ly3.png)
![](https://i.imgur.com/GG00wJ3.png)

* A próxima etapa é criar um serviço no cloud run para o projeto clicando em cloud run > criar serviço:

![](https://i.imgur.com/iV6emrH.png)
![](https://i.imgur.com/HB23CCu.png)

* Selecione a imagem do build do container, clique em configuração avançada > variáveis e secrets > e coloque um valor para a variável de ambiente service:

![](https://i.imgur.com/s6QpWJi.png)
![](https://i.imgur.com/W2qsMOE.png)


* clique em Permitir invocações não autenticadas e clique em criar:

![](https://i.imgur.com/npfON3H.png)

pronto! Agora é só acessar seu serviço na interface do serviço da Cloud Run! :)



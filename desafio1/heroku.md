# Desafio 1 - A
## Deploy no Heroku
* Para este desafio primeiro façamos o login no heroku no seu terminal com o comando:


```
heroku login
```


* Após isso abra o seu dashboard do heroku e crie uma nova aplicação em new:
![](https://i.imgur.com/rKM2Yzp.png)


* De um nome para a sua aplicação e click em create app:
![](https://i.imgur.com/m9tTxJv.png)


* Em deploy siga os passos necessários:
![](https://i.imgur.com/ydklyh4.png)


* Entre dentro do projeto de teste com o terminal:


```
cd python-backend
```


* Utilize o comando a seguir para conctar o repo no heroku no seu repo local:


```
 heroku git:remote -a deploy-teste-kyros

```


* Crie um arquivo chamado Procfile na raiz do projeto. Dentro do arquivo coloque o seguinte script:


```
web: gunicorn app:app
```


Agora é só adicionar as alterações ao git:


```
git add .
git commit -am "my first deploy - heroku"

```


* De volta ao navegador adicione as variáveis de ambiente necessárias e click em add:
![](https://i.imgur.com/csY4cTl.png)


* Agora é só executar o último comando no terminal:


```
git push heroku master
```


* Após o fim do processo de deploy que você pode acompanhar pelo seu terminal é só clicar no botão de open app no canto superior direito da interface do dashboard da heroku:

![](https://i.imgur.com/EN2WyKG.png)

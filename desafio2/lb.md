
# Desafio 2
## Load Balance e Google Cloud Plataform
* Para o desafio de load balancer primeiro criamos uma virtual machine na google cloud acessando o compute engine > instâncias de VM:
![](https://i.imgur.com/q1xXC60.png)

* Crie uma nova instância:
![](https://i.imgur.com/nVbcFlR.png)

* crie uma máquina do tipo F1-micro e ubuntu 20.04LTS:
![](https://i.imgur.com/CU69g61.png)
![](https://i.imgur.com/dgGUBY4.png)

OBS:
Para nós utilizarmos todos os recursos desse desafio é necessário liberar conexão as portas da VM.
Para isso vamos em Ver detalhes de Redes > firewall:
![](https://i.imgur.com/ysBYwju.png)
![](https://i.imgur.com/4snEHcF.png)

* Clique em Criar uma nova regra de firewall:

![](hhttps://i.imgur.com/vI9xwtv.png)

* Escolha a configuração necessária para abrir:

![](https://i.imgur.com/wh4jPSp.png)

* abra uma conexão SSH com a VM na cloud clicando no SSH:
![](https://i.imgur.com/eU72BAK.png)

* Execute um sudo apt update e em seguida instale o docker e o vim. Logo após isso crie um arquivo de docker-compose.yml com o seguinte script:


```
version: '3'
 
services:
 nginx:
   image: nginx:1.17.6-alpine
   container_name: nginx
   ports:
     - "8000:80"
 
 pythonA:
   image: nginx:1.17.6-alpine
   container_name: pythonA
   ports:
     - "80"
 
 
 pythonB:
   image: nginx:1.17.6-alpine
   container_name: pythonB
   ports:
     - "80"

```


* agora no terminal da cloud utilize os seguintes comandos:


```
$ docker-compose up -d
$ docker-compose exec nginx apk add bash

```


* Entre dentro do NGINX e instale o vim:



```
$ docker-compose exec nginx bash
$ apk add vim

```


* Altere o arquivo de configuração do nginx que se encontra em /etc/nginx/conf.d/default.conf:



```
upstream servicos {
  server pythonA;
  server pythonB;
}
 
server {
 listen        80;
 server_name   localhost;
 
 location / {
   proxy_pass http://servicos;
 }
}

```



* saia do nginx e volte para o linux com exit. Voltando ao linux nós vamos instalar o vim e o bash no ambiente do pythonA e pythonB.



```
$ docker-compose exec pythonA apk add bash vim && docker-compose exec pythonB apk add bash vim
```


* Entre dentro do pythonA com o terminal bash:



```
$ docker-compose exec pythonA bash

```



* Dentro do pythonA vamos alterar o index.html que se encontra em /usr/share/nginx/html/index.html:



```
<h1> Servico A </h1>

```


* Vamos fazer o mesmo procedimento que fizemos acima com o pythonA iremos fazer com o pythonB.
* Agora é só executar o comando abaixo no terminal da cloud para reiniciar o container nginx e acessar a url publica na porta 8000 que fica disponível no google cloud:



```
docker restart nginx
```


### CONTEÚDO CRIADO COM GITLAB/GITBOOK

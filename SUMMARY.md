# Summary

* [Introdução](README.md)
* [Deploy no Heroku](desafio1/heroku.md#Deploy-no-Heroku)
* [Deploy no Google Cloud Plataform - Cloud Run](desafio1/gcp.md#Deploy-no-Google-Cloud-Plataform--Cloud-Run)
* [Load Balance e Google Cloud Plataform](desafio2/lb.md#Load-Balance-e-Google-Cloud-Plataform)


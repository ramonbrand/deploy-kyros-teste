# Introdução
Os desafios foram todos realizados conforme foram solicitados, respeitando a regra de negocio imposta dentro da aplicação. Espero que este gitBook seja claro quanto as explicações aqui apresentadas.

[Link para o vídeo dos desafios](https://www.youtube.com/watch?v=_OohEQrKVRI&ab_channel=RamonBrandi)

* Redes Sociais:
1. [Linkedin](https://www.linkedin.com/in/ramonbrandi/)
2. [Github](https://github.com/RamonBrandi)
3. [GitLab](https://gitlab.com/ramonbrand)

